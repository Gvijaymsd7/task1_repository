package com.greatlearning.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.greatlearning.implementetion.TopRatedMovies;
import com.greatlearning.movie.Movie;

class TopRatedMoviesTest{
	@Test
	void testGetMovie() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		List<Movie> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	@Test
	void testAdd() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setId(1);
		m.setName("narniya");
		int success=mc.add(m);
		assertEquals(1,success);
	}

	@Test
	void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setId(1);
		m.setName("bhahubali");
		mc.update(m);
		assertTrue(m.getName()=="bhahubali");
	}

	@Test
	void testDelete() throws SQLException {
		//fail("Not yet implemented");
		TopRatedMovies mc=new TopRatedMovies();
		Movie m=new Movie();
		m.setName("dangal");
		mc.delete(m);
		assertTrue(m.getName()=="dangal");
	}

}
